export default {
  header: {
    installButton: 'Установить CryptoATM',
  },
  top: {
    title: 'Криптомат от Uton',
    subTitle: 'Зарабатывай с каждой транзакции',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    installButton: 'Установить CryptoATM',
  },
  features: {
    support: {
      title: 'Техническая поддержа 24/7',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
    percents: {
      title: 'Автоматическое начисление процентов',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
    speed: {
      title: 'Высокая скорость транзакции',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
  },
  howItWorks: {
    title: 'Как работает CryptoATM',
    description: 'Клиент приходит в терминал, по оферте размещенной в терминале юридически он дает вам поручение на покупку для него Биткоина, вносит деньги, после чего с вашего биткоин адреса списываются биткоины на его адрес, а на бирже выставляется ордер на покупку такого же количества BTC, отправленного клиенту за минусом вашего вознаграждения.',
    benefitsTitle: 'Преймушество для пользователя:',
    speed: 'Скорость транзакций',
    comfort: 'Удобство в использовании',
    unlimited: 'Нет ограничений на обьем транзакций',
  },
  widget: {
    title: 'Пример',
    inLabel: 'Клиент купил BTC на',
    inDescription: 'Комиссия в терминале стоит 6%. Вы ему отправили биткоин на сумму {price}$. На бирже выставился ордер на покупку BTC на {price}$.',
    outLabel: 'Ваша прибыль',
    value: '= {price}$',
    outDescription: 'За одну транзакцию',
  },
  future: {
    title: 'Будущее финансов в руках криптовалюты',
    description: 'Тест о том как быстро рынок криптовалют развивается.  Клиент приходит в терминал, по оферте размещенной в терминале юридически он дает вам поручение на покупку для него Биткоина, вносит деньги, после чего с вашего биткоин адреса списываются биткоины на его адрес, а на бирже выставляется ордер на покупку такого же количества BTC, отправленного клиенту за минусом вашего вознаграждения.',
  },
  comparison: {
    wallets: {
      title: '> 35 миллионов<br>Bitcon-кошельков',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      before: {
        title: 'Владельцы Bitcoin кошельков  на 2017 год',
        bar: '> {value} пользователей'
      },
      after: {
        title: 'Владельцы Bitcoin кошельков на 2018 год',
        bar: '> {value} пользователей',
      }
    },
    capitalization: {
      title: 'Мировая капитализация<br>Bitcoin >110 миллиардов USD',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      before: {
        title: 'Мировая капитализация в 2017 году',
        bar: '> {value} USD'
      },
      after: {
        title: 'Мировая капитализация в 2017 году',
        bar: '> {value} USD'
      }
    },
    transactions: {
      title: '> 870 000<br>транзакций в день',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      before: {
        title: 'Средние количество транзакций в день на период 2017 года',
        bar: '> {value} транзакций'
      },
      after: {
        title: 'Средние количество транзакций в день на период 2018 года',
        bar: '> {value} транзакций'
      }
    },
  },
  subscription: {
    title: 'Познакомтесь поближе с Uton',
    description: 'Оставьте свой e-mail и Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    placeholder: 'Email',
    submitButton: 'Отправить',
    socialsTitle: 'Мы в соцсетях'
  },
  franchise: {
    franchise: {
      title: 'Если Вы  хотите установить криптом и стать партнером Uton, Вам потребуется следующее:',
      register: 'Регистрация бизнеса.',
      money: 'Наличие оборотных средств, а именно:<br>— часть в BTC на Вашем биткоин адресе, привязанном к терминалу, часть в рублях на бирже.',
      installButton: 'Установить CryptoATM',
    },
    tech: {
      title: 'Техническая информация',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      downloadButton: 'Скачать PDF',
    }
  },
  modal: {
    title: 'Установить CryptoATM',
    description: 'Клиент приходит в терминал, по оферте размещенной в терминале юридически он дает вам поручение на покупку для него Биткоина, вносит деньги, после чего с вашего биткоин адреса списываются биткоины на его адрес, а на бирже выставляется ордер на покупку такого же количества BTC, отправленного клиенту за минусом вашего вознаграждения.',
    name: 'Имя',
    email: 'E-mail',
    phone: 'Номер телефона',
    submitButton: 'Отправить',
    agreement: 'Отправляя форму, я даю согласие на {link}',
    agreementLink: 'обработку персональных данных'
  }
}
