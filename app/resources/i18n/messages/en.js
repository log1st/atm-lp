export default {
  header: {
    installButton: 'Install CryptoATM',
  },
  top: {
    title: 'Uton Crypto ATM',
    subTitle: 'Earn with Each Transaction',
    description: 'We are glad to present you the best BTM hardware and software in the industry. The UTON ATM is the whole infrastructure that combines all your ATMs in a single network manageable from within the mobile application or the web admin panel. It allows you to provide the best user experience to your customers and to be sure that you will know almost everything that happens with your ATM right now.',
    installButton: 'Install CryptoATM',
  },
  features: {
    support: {
      title: 'Technical support 24/7',
      description: 'We offer the support for the ATM owners. Most of things is automatic, but if you need assistance with our ATMs, we are glad to help you!',
    },
    percents: {
      title: 'Automatic accrual of interest',
      description: 'The ATM calculates the commission on transactions and your interest is saved automatically on all your ATMs in cash or in crypto assets. All you need to do is to withdraw it from time to time.',
    },
    speed: {
      title: 'High transaction speed',
      description: 'We offer ability to initiate cash withdrawal transaction remotely so client don’t need to wait long time for crypto assets like Bitcoin to fulfil required amount transaction confirmations while he is waiting for the transaction.',
    },
  },
  howItWorks: {
    title: 'How it works',
    description: 'A client comes to the ATM, he legally gives you an order to buy Bitcoin for him an by offer and acceptance placed in the ATM, pays money, and then bitcoins are being transferred from your address to his address. Later an order for the purchase of the same quantity (minus your commission) of BTC as was sent to a customer is placed at the exchange.',
    benefitsTitle: 'Benefits for users:',
    speed: 'Transactions speed',
    comfort: 'Ease of use',
    unlimited: 'Configurable transactions amount limit',
  },
  widget: {
    title: 'Example',
    inLabel: 'A customer bought a BTC for',
    inDescription: 'The commission of the terminal is set as 6%. You send a bitcoin worth USD ${price}. to a customer. In a background an order for the purchase of BTC for USD ${price} issued at the exchange.',
    outLabel: 'Your profit',
    value: '= {price}$',
    outDescription: 'Per transaction',
  },
  future: {
    title: 'The future of finance is a cryptocurrency',
    description: 'We offer you an example to show how rapid cryptocurrency market is developing. In 2009 Bitcoin was released, and in several years after that a lot of other cryptocurrencies came in.<br>Cryptocurrencies break borders between countries and financial markets. That is what we have today. We are joining fiat money market and cryptocurrency.',
  },
  comparison: {
    wallets: {
      title: '> 35 million<br>Bitcoin Wallets',
      description: 'Amount of users who use the Bitcoin and other cryptocurrencies grows day after day. Some want to get money by investing, others find other usage. Nowadays it is so wide spread so you can even buy food for Bitcoin just as with fiat money.',
      before: {
        title: 'Bitcoin owners for 2017',
        bar: '> {value} users'
      },
      after: {
        title: 'Bitcoin owners for 2018',
        bar: '> {value} users',
      }
    },
    capitalization: {
      title: 'World capitalization of<br>Bitcoin USD $110 billion',
      description: 'The market of cryptocurrencies is constantly growing and people are constantly investing fiat money, transferring them into world of cryptocurrencies.',
      before: {
        title: 'World capitalization in 2017',
        bar: '> {value} USD'
      },
      after: {
        title: 'World capitalization in 2018',
        bar: '> {value} USD'
      }
    },
    transactions: {
      title: '> 870 000<br>transactions per day',
      description: 'As the network and count of users of the Bitcoin grows, the amount of transactions is grows too. And the growth is fast from the last year to the current it has almost increased twice.',
      before: {
        title: 'Average count of transactions in 2017',
        bar: '> {value} transactions'
      },
      after: {
        title: 'Average count of transactions in 2018',
        bar: '> {value} transactions'
      }
    },
  },
  subscription: {
    title: 'Do you want to know more?',
    description: 'Leave your e-mail',
    placeholder: 'Email',
    submitButton: 'Send',
    socialsTitle: 'We in social networks',
    submitted: 'Your request was submitted successfully. A sales representative will contact you shortly.'
  },
  franchise: {
    franchise: {
      title: 'If you want to install Crypto ATM and become a partner of Uton, you will need the following:',
      register: 'Business Registration',
      money: 'The availability of working capital, namely:<br>—a part in BTC on your bitcoin address linked with the terminal, another part in one of the world’s major currencies on the exchange',
      installButton: 'Install CryptoATM',
    },
    tech: {
      title: 'Presentation',
      description: 'Here we publish the presentation document describing most important features of the UTON ATM in simple form.',
      downloadButton: 'Download PDF',
    }
  },
  modal: {
    title: 'Install the UTON ATM!',
    description: 'Fill out the information below and a sales representative will contact you shortly.',
    name: 'Name',
    email: 'E-mail',
    phone: 'Phone',
    submitButton: 'Send',
    agreement: 'By submitting the form, I agree to {link}',
    agreementLink: 'personal data processing',
    error: 'Please fill all the required fields!',
    submitted: 'Your request was submitted successfully. A sales representative will contact you shortly.'
  }
}
