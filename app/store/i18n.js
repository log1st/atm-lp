export const state = () => ({
  language: 'en',
  languagesList: ['en'],
  languagesMap: {
    en: {
      key: 'en',
      locale: 'en-US',
      name: 'English'
    },
    ru: {
      key: 'ru',
      locale: 'ru-RU',
      name: 'Русский'
    }
  }
});

export const getters = {
  languages({languagesList, languagesMap}) {
    return languagesList.map(language => languagesMap[language]);
  },
};

export const MUTATION_SET_LANGUAGE = 'i18n/setLanguage';

export const mutations = {
  [MUTATION_SET_LANGUAGE.replace('i18n/', '')](state, language) {
    state.language = language
  }
};
