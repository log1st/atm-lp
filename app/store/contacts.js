export const state = () => ({
  phones: [
    '+86 135 109 09 172',
    '+86 185 66 74 74 00',
    {text: '+33 635 43 22 24', icon: 'telegram'}
  ],
  secondPhone: '+7 924 200-64-80',
  email: 'hi@utonwallet.io',
  city: 'Hong Kong',
  address: 'Kowloon Bay, Lam Lok St,<br>Nam Fung Commercial Centre'
});

