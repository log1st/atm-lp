import {MUTATION_SET_LANGUAGE} from "./i18n";

export const state = () => ({
  isActive: false
});

export const MUTATION_SHOW_MODAL = 'modal/show';
export const MUTATION_HIDE_MODAL = 'modal/hide';

export const mutations = {
  [MUTATION_SHOW_MODAL.replace('modal/', '')](state) {
    state.isActive = true
  },
  [MUTATION_HIDE_MODAL.replace('modal/', '')](state) {
    state.isActive = false
  },
};
