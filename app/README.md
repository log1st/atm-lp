Dev:

```bash
npm run dev
```

Build

```bash
npm run build
```

Serve build (will hang on localhost:3000)

```
npm run build && npm start
```

Generate static (will be in ./.nuxt/dist)

```bash
npm run build && npm run generate
```
