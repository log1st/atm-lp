import ru from "./resources/i18n/messages/ru";
import en from "./resources/i18n/messages/en";

module.exports = {
  css: [
    '~assets/root.scss'
  ],
  generate: {
    routes: () => [
      '/',
    ]
  },
  head: {
    title: 'UTON ATM',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'ATM LP'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'}
    ]
  },
  modules: [
    '@nuxtjs/axios',
    ['nuxt-i18n', {
      seo: false,
      parsePages: false,
      locales: ['en'].map(item => ({
        code: item,
        file: `${item}`
      })),
      defaultLocale: 'en',
      langDir: 'resources/i18n/messages/',
      lazy: true,
      vuex: {
        moduleName: 'vuei18n',
      },
      vueI18n: {
        fallbackLocale: 'en',
        // messages: {
        //   en: en,
        // }
      },
    }]
  ],
  axios: {
    baseURL: '/'
  },
  loading: {color: '#3B8070'},
  build: {
    babel: {
      plugins: ['syntax-jsx', 'transform-vue-jsx']
    },
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
      config.module.rules.find(item => item.test.source === /\.css$/.source).oneOf = [({
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: `${isDev ? '[local]_' : ''}[hash:base64:8]`,
              camelCase: true,
            }
          }
        ]
      })];
    }
  }
}
